# RedHat Openshift Enterprise 3.1 on Azure

Please note that you will need to have ssh keys access to all the nodes.
## Create the cluster
### Create the cluster on the Azure Portal

<a href="https://portal.azure.com/#create/Microsoft.Template/uri/https://gitlab.com/lewill/openshift-azure/raw/master/azuredeploy.jsontarget="_blank">
    <img src="http://azuredeploy.net/deploybutton.png"/>
</a>
<a
href="http://armviz.io/#/?load=https://gitlab.com/lewill/openshift-azure/raw/master/azuredeploy.json" target="_blank">
    <img src="http://armviz.io/visualizebutton.png"/>
</a>

### Create the cluster with powershell

```powershell
New-AzureRmResourceGroupDeployment -Name <DeploymentName> -ResourceGroupName <RessourceGroupName> -TemplateUri https://gitlab.com/lewill/openshift-azure/blob/master/azuredeploy.json
```

## Install Openshift Enterprise 3.1 with Ansible

You must use SSH Agentforwarding.
The Installation is based on [Openshift Ansible](https://github.com/openshift/openshift-ansible).
The ARM file is based on https://github.com/derdanu/azure-openshift/,
this version suppose that you have your own Red Hat Enterprise Linux
template and images uploaded to azure.

### Bash

```bash
user@localmachine:~$ ssh -A <MasterIP>
[adminUsername@master ~]$ ./openshift-install.sh
```

------

## Parameters
### Input Parameters

| Name| Type           | Description |
| ------------- | ------------- | ------------- |
| adminUsername  | String       | Username for SSH Login and Openshift Webconsole |
|  adminPassword | SecureString | Password for the Openshift Webconsole |
| sshKeyData     | String       | Public SSH Key for the Virtual Machines |
| masterDnsName  | String       | DNS Prefix for the Openshift Master / Webconsole | 
| numberOfNodes  | Integer      | Number of Openshift Nodes to create |

### Output Parameters

| Name| Type           | Description |
| ------------- | ------------- | ------------- |
| openshift Webconsole | String       | URL of the Openshift Webconsole |
| openshift Master ssh |String | SSH String to Login at the Master |
| openshift Router Public IP | String       | Router Public IP. Needed if you want to create your own Wildcard DNS |

------

This template deploys a RedHat Openshift Enterprise cluster on Azure.
